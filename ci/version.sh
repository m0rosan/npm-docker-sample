#!/bin/bash 
set -euo pipefail

VERSION=$1

# should use ssh that's associated to deploy keys instead of user's PAT
URL=`git remote get-url origin | sed -e "s/https:\/\/gitlab-ci-token:.*@//g"`
git remote set-url origin "https://gitlab-ci-token:${PAT}@${URL}"

git config --global user.email "ci-auto@example.com"
git config --global user.name "CI User"

git add package.json package-lock.json
git commit -m "Update npm version: $VERSION"
git push origin HEAD:main -o ci.skip

# untag and move the tag to HEAD commit
git tag -d $VERSION
git tag $VERSION
# tag should trigger a pipeline 
git push origin --tags 