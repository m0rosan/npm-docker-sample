var assert = require('assert')
var arithmetic = require('./')

function testAdd() {
  assert.strictEqual(arithmetic.add(1, 2), 3)
  assert.strictEqual(arithmetic.add(1, '2'), 3)
}

testAdd()