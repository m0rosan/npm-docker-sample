# build stage
FROM node:lts-alpine as build

WORKDIR /app
COPY package*.json ./
RUN npm install --production
COPY . .

# Build asset here into /app/build

# production stage - do whatever here. copy artifacts from build stage and serve
FROM nginx:stable-alpine as production
# COPY --from=build /app/build /usr/share/nginx/html
# EXPOSE 80
CMD ["echo", "foo bar"]
