GIT_SHORT_SHA=$(shell git rev-parse --short HEAD)
GITVERSION_VERSION=1.1.6
DOCKER_IMAGE_NAME=sample

deps:
	npm install

ci-deps:
	curl -L https://github.com/screwdriver-cd/gitversion/releases/download/v${GITVERSION_VERSION}/gitversion_linux_amd64 -o /usr/local/bin/gitversion \
    && chmod +x /usr/local/bin/gitversion

test:
	npm test

config:
	npm config set git-tag-version=false 

bump: config
	$(eval VERSION=$(shell gitversion bump auto))
	npm version $(VERSION)
	ci/version.sh $(VERSION)

unreleased-publish: config
	$(eval VERSION=$(shell gitversion show))
	npm version $(VERSION)-$(GIT_SHORT_SHA)
	echo "do npm publish"
	npm publish || true

publish:
	echo "do npm publish"
	# npm publish

build-docker:
	docker build -t $(DOCKER_IMAGE_NAME):latest .

publish-docker: build-docker
	$(eval VERSION=$(shell gitversion show))
	docker tag  $(DOCKER_IMAGE_NAME):latest $(DOCKER_IMAGE_NAME):$(VERSION)
	echo "do docker publish"
	# docker push $(DOCKER_IMAGE_NAME):latest
