# sample

sample project to demonstrate a pipeline

On branch,

- it tests
- it can build a docker image

On merge,

- it tests again
- it bumps version based on commit message, use the version to bump npm package version, and move the version to head of commit
- it pushes head commit and tag

On tag,

- it publishes npm package
- it publishes docker image

## Tools

[gitversion][gitversion]: This detects major, minor, patch and releases in commit message and bump git version accordingly

## Note

this contains some bad practices.

- using user's access token in CI. Better to use Bot user like project access token or a dedicated bot user
- I don't like http. re-set origin to ssh, and use ssh key (deploy key created by a bot user so that accesss is scoped to the project)
- use all debian base or alpine base in CI
- race condition: it bails out if new commit is made during main pipeline

Please figure out yourself to set up npm client and docker login

[gitversion]:https://github.com/screwdriver-cd/gitversion
