var cowsay = require("cowsay");
var isNumber = require("is-number")
var pjson = require("./package.json")

printCowsay = function () {
  console.log(cowsay.say({
    text : "I'm moooodule",
    e : "oO",
    T : "U "
  }));
}

printMsg = function() {
  console.log("This is a message from the sample package: version", pjson.version);
}

exports.add = function add (n1, n2) {
  if (!isNumber(n1) || !isNumber(n2)) {
    throw new Error("n1 and n2 must be a number")
  }
  return (+n1) + (+n2)
}

printCowsay();
printMsg();
